import pathlib                                  


def cont(folder, files_list):                   
    for file_or_dir in folder.iterdir():        # Iterating through the directory
        if not file_or_dir.is_dir():            #checking for file
            if str(file_or_dir).endswith(".txt"):
                files_list.append(file_or_dir)
        else:
            cont(file_or_dir, files_list)               #recursivly running through sub-folders
    return files_list                                   #returning the list with the file paths


def take_out_content(list_of_files):
    content_list = []                                  #the code takes the content from
    for some_file in list_of_files:                    #each file and put it in a new list so it can AlphaBeticly sort them
        with open(some_file, "r") as file:             
            content_list.append(file.read())
    return content_list


with open("contents.txt", "a") as contents:
    for file in sorted(take_out_content(cont(pathlib.Path('.'), []))):
        contents.write(file + "\n")
